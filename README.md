# 介绍

1. 使用前点击 docker-compose.yml 中的绿色按钮进行启动
2. IDEA 启动失败可参考 https://blog.csdn.net/weixin_63594548/article/details/140962044
   或在 docker-compose.yml 文件所属目录执行 `docker-compose up`

# 其他

综合考虑下 ab 请安装在主机，而不是在容器中

```shell
sudo apt install apache2-utils
```

🙋 能不能动态切换 profile

不能