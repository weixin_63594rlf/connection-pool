# c3p0

```text
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:    17   63  29.7     54     178
Waiting:       17   63  29.7     54     177
Total:         18   63  29.7     54     178
```

# dbcp

```text
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:    21   52  31.8     42     282
Waiting:       21   52  31.8     42     281
Total:         21   52  31.8     42     282
```

# druid

```text
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:    48   83  19.1     80     200
Waiting:       47   83  19.1     80     199
Total:         48   83  19.1     80     200
```

# hikari

```text
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:    21   51  39.3     34     265
Waiting:       21   50  39.3     34     265
Total:         22   51  39.3     35     266
```

# no-pool

```text
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:    25   54  21.5     51     221
Waiting:       25   54  21.4     50     220
Total:         25   54  21.4     51     221
```