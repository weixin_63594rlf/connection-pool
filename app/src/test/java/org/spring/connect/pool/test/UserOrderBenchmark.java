package org.spring.connect.pool.test;

import jakarta.annotation.Resource;
import org.apache.commons.lang3.RandomStringUtils;
import org.spring.connect.pool.Application;
import org.spring.connect.pool.mapper.UserOrderMapper;
import org.spring.connect.pool.model.UserOrder;
import org.openjdk.jmh.annotations.*;


import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class UserOrderBenchmark {

    @Resource
    private UserOrderMapper userOrderMapper;

    @Setup
    public void setup() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Application.class);
        userOrderMapper = applicationContext.getBean(UserOrderMapper.class);
    }

    @Benchmark
    @GroupThreads(20) // 每次 20 个线程
    public void testInsert() {
        UserOrder userOrder = UserOrder.builder()
                .userName("用户名")
                .userId("用户名".concat(RandomStringUtils.randomNumeric(3)))
                .userMobile("+86 123456789".concat(String.format("%04d", new Random().nextInt(1000))))
                .sku("123456789")
                .skuName("Spring Cloud")
                .orderId(RandomStringUtils.randomNumeric(11))
                .quantity(1)
                .unitPrice(BigDecimal.valueOf(128))
                .discountAmount(BigDecimal.valueOf(50))
                .tax(BigDecimal.ZERO)
                .totalAmount(BigDecimal.valueOf(78))
                .orderDate(new Date())
                .orderStatus(0)
                .isDelete(0)
                .uuid(UUID.randomUUID().toString().replace("-", ""))
                .ipv4("127.0.0.1")
                .ipv6("2001:0db8:85a3:0000:0000:8a2e:0370:7334".getBytes())
                .extData("{\"device\": {\"machine\": \"IPhone 14 Pro\", \"location\": \"shanghai\"}}")
                .build();
        userOrderMapper.insert(userOrder);
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(UserOrderBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(options).run();
    }
}
