package org.spring.connect.pool;

import org.spring.connect.pool.mapper.UserOrderMapper;
import org.spring.connect.pool.model.UserOrder;
import com.google.common.base.Stopwatch;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@SpringBootApplication
@RestController("mysql")
@RequestMapping("/api/mysql/")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Resource
    private UserOrderMapper userOrderDao;


    /// ```shell
    /// schema="http://"; host="127.0.0.1:8091"; path="/api/mysql/insert"; url="${schema}${host}${path}"; ab -c 20 -n 1000 "$url"
    ///```
    @RequestMapping("insert")
    public String insert() {
        UserOrder userOrder = UserOrder.builder()
                .userName("用户名")
                .userId("用户名".concat(RandomStringUtils.randomNumeric(3)))
                .userMobile("+86 123456789".concat(String.format("%04d", new Random().nextInt(1000))))
                .sku("123456789")
                .skuName("Spring Cloud")
                .orderId(RandomStringUtils.randomNumeric(11))
                .quantity(1)
                .unitPrice(BigDecimal.valueOf(128))
                .discountAmount(BigDecimal.valueOf(50))
                .tax(BigDecimal.ZERO)
                .totalAmount(BigDecimal.valueOf(78))
                .orderDate(new Date())
                .orderStatus(0)
                .isDelete(0)
                .uuid(UUID.randomUUID().toString().replace("-", ""))
                .ipv4("127.0.0.1")
                .ipv6("2001:0db8:85a3:0000:0000:8a2e:0370:7334".getBytes())
                .extData("{\"device\": {\"machine\": \"IPhone 14 Pro\", \"location\": \"shanghai\"}}")
                .build();

        Stopwatch stopwatch = Stopwatch.createStarted();
        userOrderDao.insert(userOrder);
        stopwatch.stop();

        log.info("性能测试，insert 耗时: {}", stopwatch);
        return "done! 耗时：" + stopwatch;
    }


}
