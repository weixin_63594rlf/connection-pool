package org.spring.connect.pool.mapper;

import org.spring.connect.pool.model.UserOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserOrderMapper {

    void insert(UserOrder userOrder);

    void updateOrderStatusByUserId(String userId);

    void updateOrderStatusByUserMobile(String userMobile);

    void updateOrderStatusByOrderId(String orderId);

    UserOrder selectById(Long id);

    List<UserOrder> selectByUserId(String userId);

    List<UserOrder> selectByUserMobile(String userMobile);

    UserOrder selectByOrderId(String orderId);

    UserOrder selectByOrderIdAndUserId(UserOrder userOrder);

    UserOrder selectByUserIdAndOrderId(UserOrder userOrder);

    Long queryMaxId();

}
